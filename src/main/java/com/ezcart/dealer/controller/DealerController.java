package com.ezcart.dealer.controller;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;




@RestController
@CrossOrigin
public class DealerController {

	
	@PostMapping("/upload")
	public ResponseEntity<?> uploadToLocalFileSystem(@RequestParam("file") MultipartFile file) throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException, IOException, InterruptedException {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//		System.err.println(fileName);
		
		
//		String filePath ="C:\\Users\\santhosh\\Desktop\\EzCart\\ezcartdealer\\src\\main\\resources\\data\\"; 
		
		try {
			String dir = "C:\\Users\\santhosh\\Desktop\\EzCart\\ezcartdealer\\target\\classes\\documents\\";
			Path paths = Paths.get(dir);
			Files.createDirectories(paths);
			String filePath ="C:\\Users\\santhosh\\Desktop\\EzCart\\ezcartdealer\\target\\classes\\documents\\";
			Path path = Paths.get(filePath + fileName);
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/files/download/")
				.path(fileName)
				.toUriString();
//		excelToDbScheduler.jobScheduled();
		return ResponseEntity.ok(fileDownloadUri);
	}
	
}
