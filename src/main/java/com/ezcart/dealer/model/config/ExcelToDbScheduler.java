package com.ezcart.dealer.model.config;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ExcelToDbScheduler {

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	Job job;

	@Autowired
	ExcelToDbConfig excelToDbConfig;

	@Scheduled(cron = "*/10 * * * * *")
	public void jobScheduled() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException, IOException, InterruptedException {
			Resource[] resources = null;
		    ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();   
		    try {
		        resources = patternResolver.getResources("classpath:documents\\*.csv");
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		    
		    for(int itr=0; itr<resources.length;itr++) {
		    	if(!resources[itr].getFilename().equals("Santhosh.xlsx")) {
		      File file = new File("C:\\Users\\santhosh\\Desktop\\EzCart\\ezcartdealer\\target\\classes\\documents\\"+ resources[itr].getFilename());
		    Map<String, JobParameter> maps = new HashMap<String, JobParameter>();
			maps.put("time", new JobParameter(System.currentTimeMillis()));
			JobParameters parameters = new JobParameters(maps);

			JobExecution jobExecution = jobLauncher.run(job, parameters);
			
			if (!jobExecution.getStatus().isUnsuccessful()) {
				file.delete();
				System.err.println("DELETED");
			}
		}
		    }
	}
}
