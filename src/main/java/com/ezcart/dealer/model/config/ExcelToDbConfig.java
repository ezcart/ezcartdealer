package com.ezcart.dealer.model.config;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.net.MalformedURLException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.excel.ExcelFileParseException;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.mapping.BeanWrapperRowMapper;
import org.springframework.batch.item.excel.poi.PoiItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import com.ezcart.dealer.model.Processor;
import com.ezcart.dealer.model.Products;


@Component
public class ExcelToDbConfig {
	

	  	
	  	@Autowired
	  	StepBuilderFactory builderFactory;
	  
	   public String PROPERTY_EXCEL_SOURCE_FILE_PATH = "excel.to.database.job.source.file.path";
		
	  	@Bean
		public Job job(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,ItemWriter<Products> itemWritter) throws UnexpectedInputException, ParseException,ExcelFileParseException, NonTransientResourceException, Exception 
		{
			Step step =stepBuilderFactory.get("CSV-FILE-LOAD")
					.<Products, Products>chunk(1)
					.reader(multiResourceItemReader())
					.processor(processor())
					.writer(itemWritter)
					//.taskExecutor(taskExecutor())
					.build();	
			return jobBuilderFactory.get("CSV-LOAD")
			.incrementer(new RunIdIncrementer())
			.start(step)
			.build();
			
		}
	

		   @Bean
		   public MultiResourceItemReader<Products> multiResourceItemReader() throws IOException
		   {
			   Resource[] resources = null;
			   ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();   
		       resources = patternResolver.getResources("classpath:documents\\*.csv");
		       MultiResourceItemReader<Products> resourceItemReader = new MultiResourceItemReader<Products>();
		       resourceItemReader.setResources(resources);
		       resourceItemReader.setDelegate(reader());
		       return resourceItemReader;
		   }

		   @Bean
		   public FlatFileItemReader<Products> reader() 
		   {
		       //Create reader instance
		       FlatFileItemReader<Products> reader = new FlatFileItemReader<Products>();
		       reader.setLinesToSkip(1);   
		       reader.setLineMapper(lineMapper());
		       return reader;
		   }
		   
		   @Bean 
			public LineMapper<Products> lineMapper(){
				
				DefaultLineMapper<Products> defaultLineMapper = new DefaultLineMapper<>();
				DelimitedLineTokenizer lineTockenizer = new DelimitedLineTokenizer();
				lineTockenizer.setStrict(false);
				lineTockenizer.setNames(new String[] {"productsId","productName","productType","productPrice","produtVariant","productColour","productImage"});
				BeanWrapperFieldSetMapper<Products> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
				fieldSetMapper.setTargetType(Products.class);
				defaultLineMapper.setLineTokenizer(lineTockenizer);
				defaultLineMapper.setFieldSetMapper(fieldSetMapper);
				return defaultLineMapper;
			}
		   
	  
		@Bean
		public ItemProcessor<Products,Products> processor(){
			return new Processor();
		}
		
			
		@Bean
		public JdbcBatchItemWriter<Products> itemWriter(DataSource dataSource) {
		  return new JdbcBatchItemWriterBuilder<Products>()
				  .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Products>())
				  .itemPreparedStatementSetter(setter())
		    .sql("REPLACE into products(product_id,product_name,product_type,product_price,product_variant,product_colour,product_image) values(?,?,?,?,?,?,?)")
		    .dataSource(dataSource)
		    .build();
			//System.err.println("AFTER");
		}
	
		
		
	    @Bean
	    public ItemPreparedStatementSetter<Products> setter() {
	        return new ItemPreparedStatementSetter<Products>() {
				public void setValues(Products rs, PreparedStatement ps) throws SQLException {
					ps.setString(2,rs.getProductName());
					ps.setString(3,rs.getProductType());
					ps.setString(5,rs.getProductColour());
					ps.setString(7,rs.getProductVariant());
					ps.setString(6,rs.getProductImage());
					ps.setFloat(4,rs.getProductPrice());
					ps.setString(1, rs.getProductId());
					
				}
			};

	    }        
	        


	    

}